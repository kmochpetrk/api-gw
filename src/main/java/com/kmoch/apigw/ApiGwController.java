package com.kmoch.apigw;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import org.springframework.http.*;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.kmoch.apigw.SecurityConstants.*;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/apigw")
public class ApiGwController {

    private Map<String, ServiceToRegister> mapOfRegisteredServices = new HashMap<>();

    private RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    public void postConstruct() {
        mapOfRegisteredServices.put("service1", new ServiceToRegister(
                "http://localhost:8081/apigw/service1", null, HttpMethod.GET, null));

//        mapOfRegisteredServices.put("service2", new ServiceToRegister(
//                "http://localhost:8081/apigw/service2", null, HttpMethod.GET, null));
    }

    @RequestMapping(value = "/api/{serviceId}")
    public ResponseEntity<String> callFunction(@PathVariable String serviceId) {
        String rawPath = serviceId;
        for (Map.Entry<String, ServiceToRegister> entry : mapOfRegisteredServices.entrySet()) {
            if(rawPath.contains(entry.getKey())) {
                HttpHeaders httpHeaders = new HttpHeaders();
                final String accessToken = createAccessToken();
                httpHeaders.add(HEADER_STRING, TOKEN_PREFIX + accessToken);
                httpHeaders.add("user", "user");
                httpHeaders.add("passwd", "passwd");
                HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
                try {
                    return restTemplate.exchange(entry.getValue().getUrl(), entry.getValue().getHttpMethod(), httpEntity, String.class);
                } catch (Exception exc) {
                    return new ResponseEntity<>("Forbidden", HttpStatus.FORBIDDEN);
                }
            }
        }
        return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/register", method = POST)
    public ResponseEntity registerService(@RequestBody ServiceToRegister serviceToRegister) {
        String[] split = serviceToRegister.getUrl().split("/");
        mapOfRegisteredServices.put(split[split.length - 1], serviceToRegister);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/service1")
    public ResponseEntity calledService1(HttpServletRequest request) {
        if (!checkAuthHeader(request)) {
            return new ResponseEntity("Forbidden", HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity("Service1", HttpStatus.OK);
    }

    @RequestMapping(value = "/service2")
    public ResponseEntity calledService2(HttpServletRequest request) {
        if (!checkAuthHeader(request)) {
            return new ResponseEntity("Forbidden", HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity("Service2", HttpStatus.OK);
    }

    private boolean checkAuthHeader(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        Verification require = JWT.require(HMAC512("SecretKeyToGenJWTs".getBytes()));
        try {
            DecodedJWT decode = JWT.decode(token.substring(7));

            System.out.print(decode.getSignature());
            require.build().verify(token.substring(7));
        } catch (Exception exc) {
            return false;
        }
        return token != null;
    }

    private String createAccessToken() {
        String token = JWT.create()
                .withSubject("user")
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));
        return token;
    }
}
