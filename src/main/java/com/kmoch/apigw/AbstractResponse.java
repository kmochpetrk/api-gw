package com.kmoch.apigw;

import org.springframework.web.bind.annotation.ResponseBody;

public abstract class AbstractResponse<T> implements ResponseBody {

    private T response;

    public AbstractResponse(T response) {
        this.response = response;
    }

    public T getResponse() {
        return response;
    }
}
