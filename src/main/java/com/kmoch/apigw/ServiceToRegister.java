package com.kmoch.apigw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

@Value
@AllArgsConstructor
public class ServiceToRegister {
    private String url;
    private HttpHeaders httpHeaders;
    private HttpMethod httpMethod;
    private String[] params;
}
