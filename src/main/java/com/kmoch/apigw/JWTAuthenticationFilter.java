package com.kmoch.apigw;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthenticationFilter extends OncePerRequestFilter {
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest req,
//                                                HttpServletResponse res) throws AuthenticationException {
//        try {
//
//
//            return this.getAuthenticationManager().authenticate(
//                    new UsernamePasswordAuthenticationToken(
//                            "user",
//                            "passwd",
//                            new ArrayList<>())
//            );
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

//    @Override
//    protected void successfulAuthentication(HttpServletRequest req,
//                                            HttpServletResponse res,
//                                            FilterChain chain,
//                                            Authentication auth) throws IOException, ServletException {
//        SecurityContextHolder.getContext().setAuthentication(auth);
//
//        chain.doFilter(req, res);
////        String token = JWT.create()
////                .withSubject(((User) auth.getPrincipal()).getUsername())
////                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
////                .sign(HMAC512(SECRET.getBytes()));
////        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
//    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String user = request.getHeader("user");
        String passwd = request.getHeader("passwd");
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, passwd, new ArrayList<>());
//        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(this.authenticationManager.authenticate(authentication));

        filterChain.doFilter(request, response);
    }
}
